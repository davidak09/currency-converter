package com.davidak.currencyconverter

import akka.http.scaladsl.model.DateTime

object MessagesToConvert {

  trait MessageToConvert {
    def currency: String
    def amount: BigDecimal
    def isoDate: String
  }

  final case class TradeMessage(
      marketId: Long,
      selectionId: Long,
      odds: BigDecimal,
      stake: BigDecimal,
      currency: String,
      date: DateTime
  ) extends MessageToConvert {
    override def amount: BigDecimal = stake
    override def isoDate: String    = date.toIsoDateString
  }

  final case class SimpleMessage(currency: String, amount: BigDecimal, date: DateTime = DateTime.now)
      extends MessageToConvert {
    override def isoDate: String = date.toIsoDateString
  }

}
