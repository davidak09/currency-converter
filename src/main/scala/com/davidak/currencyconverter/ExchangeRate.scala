package com.davidak.currencyconverter

import akka.actor.typed.scaladsl.{ActorContext, Behaviors, StashBuffer}
import akka.actor.typed.{ActorSystem, Behavior}
import akka.http.scaladsl.Http
import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport._
import akka.http.scaladsl.model.HttpRequest
import akka.http.scaladsl.unmarshalling.Unmarshal
import com.davidak.currencyconverter.MessagesToConvert.{MessageToConvert, SimpleMessage, TradeMessage}
import spray.json.DefaultJsonProtocol._
import spray.json.RootJsonFormat

import scala.concurrent.{ExecutionContextExecutor, Future}
import scala.util.{Failure, Success}

object ExchangeRate {

  trait Command

  private final case class InitialState(fetchResponse: Either[String, FetchResponse]) extends Command
  case object Passivate                                                               extends Command

  final case class FetchResponse(exchangeRatesSource: String, exchangeRate: BigDecimal)

  final case class ExchangeRateDotHostResponse(
      success: Boolean,
      base: String,
      date: String,
      rates: Map[String, BigDecimal]
  )
  final case class DataFixerDotIoResponse(
      success: Boolean,
      base: String,
      date: String,
      rates: Map[String, BigDecimal]
  )

  def apply(
      baseCurrency: String,
      targetCurrency: String,
      date: String
  ): Behavior[Command] = {
    Behaviors.withStash(100) { buffer =>
      Behaviors.setup[Command] { context =>
        new ExchangeRate(context, buffer, baseCurrency, targetCurrency, date).start()
      }
    }
  }
}

class ExchangeRate(
    context: ActorContext[ExchangeRate.Command],
    buffer: StashBuffer[ExchangeRate.Command],
    baseCurrency: String,
    targetCurrency: String,
    date: String
) {
  import CurrencyConverterManager.{ConvertMessage, ExchangeRateResponse, GetExchangeRate, MessageConverted}
  import ExchangeRate._

  implicit val system: ActorSystem[Nothing] = context.system
  // needed for the future flatMap/onComplete in the end
  implicit val executionContext: ExecutionContextExecutor = system.executionContext
  implicit val exchangeRateDotHostResponseFormat: RootJsonFormat[ExchangeRateDotHostResponse] = jsonFormat4(
    ExchangeRateDotHostResponse
  )
  implicit val dataFixerDotIoResponseFormat: RootJsonFormat[DataFixerDotIoResponse] = jsonFormat4(
    DataFixerDotIoResponse
  )

  private val DataFixerDotIoApiKey = "50c3b0cb4fca1a8b141e7b12345c3c75"

  private def start(): Behavior[Command] = {
    context.pipeToSelf(fetchExchangeRates()) {
      case Success(fetchResponse) => InitialState(fetchResponse)
      case Failure(cause) =>
        InitialState(
          Left(
            s"Cannot fetch rates for base: $baseCurrency, currency: $targetCurrency, date: $date: ${cause.getMessage}"
          )
        )
    }

    Behaviors
      .receiveMessage {
        case InitialState(fetchResponse) =>
          // now we are ready to handle stashed messages if any
          buffer.unstashAll(active(fetchResponse))
        case other =>
          // stash all other messages for later processing
          buffer.stash(other)
          Behaviors.same
      }
  }

  private def active(fetchResponse: Either[String, FetchResponse]): Behavior[Command] = {
    Behaviors
      .receiveMessagePartial {
        case ConvertMessage(messageToConvert, replyTo) =>
          replyTo ! convertMessage(messageToConvert, fetchResponse)
          Behaviors.same
        case GetExchangeRate(_, _, replyTo) =>
          replyTo ! ExchangeRateResponse(fetchResponse)
          Behaviors.same
        case Passivate =>
          Behaviors.stopped
      }

  }

  private def fetchExchangeRates(): Future[Either[String, FetchResponse]] = {
    fetchExchangeRatesFromExchangeRateDotHost()
      .recoverWith { case _ => fetchExchangeRatesFromDataFixerDotIo() }
  }

  private def fetchExchangeRatesFromExchangeRateDotHost(): Future[Either[String, FetchResponse]] = {
    println(
      s"fetching rates from https://api.exchangerate.host - date: $date, base: $baseCurrency, symbols: $targetCurrency"
    )
    Http()
      .singleRequest(
        HttpRequest(uri = s"https://api.exchangerate.host/$date?base=$baseCurrency&symbols=$targetCurrency")
      )
      .flatMap { res =>
        Unmarshal(res).to[ExchangeRateDotHostResponse]
      }
      .map(res =>
        if (res.success && res.rates.contains(targetCurrency))
          Right(FetchResponse("https://api.exchangerate.host", res.rates(targetCurrency)))
        else
          Left("Fetching rates from https://api.exchangerate.host was not successful")
      )
  }

  private def fetchExchangeRatesFromDataFixerDotIo(): Future[Either[String, FetchResponse]] = {
    println(
      s"fetching rates from https://data.fixer.io - date: $date, base: $baseCurrency, symbols: $targetCurrency"
    )
    Http()
      .singleRequest(
        HttpRequest(uri =
          s"https://data.fixer.io/api/$date?access_key=$DataFixerDotIoApiKey&base=$baseCurrency&symbols=$targetCurrency"
        )
      )
      .flatMap { res =>
        Unmarshal(res).to[DataFixerDotIoResponse]
      }
      .map(res =>
        if (res.success && res.rates.contains(targetCurrency))
          Right(FetchResponse("https://data.fixer.io", res.rates(targetCurrency)))
        else
          Left("Fetching rates from https://data.fixer.io was not successful")
      )
  }

  private def convertMessage(
      messageToConvert: MessageToConvert,
      fetchResponse: Either[String, FetchResponse]
  ): MessageConverted = {
    fetchResponse match {
      case Left(error) => MessageConverted(messageToConvert, Left(error))
      case Right(value) =>
        val convertedValue =
          (messageToConvert.amount / value.exchangeRate).setScale(5, BigDecimal.RoundingMode.HALF_UP)
        val convertedMessage = messageToConvert match {
          case tradeMessage: TradeMessage   => tradeMessage.copy(currency = baseCurrency, stake = convertedValue)
          case simpleMessage: SimpleMessage => simpleMessage.copy(currency = baseCurrency, amount = convertedValue)
        }
        MessageConverted(messageToConvert, Right(convertedMessage))
    }
  }
}
