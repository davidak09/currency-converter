package com.davidak.currencyconverter

import akka.actor.typed._
import akka.actor.typed.scaladsl.{AbstractBehavior, ActorContext, Behaviors}
import akka.http.scaladsl.model.DateTime
import com.davidak.currencyconverter.ExchangeRate.FetchResponse
import com.davidak.currencyconverter.MessagesToConvert.MessageToConvert

import scala.concurrent.ExecutionContextExecutor
import scala.concurrent.duration._

object CurrencyConverterManager {

  def apply(): Behavior[Command] =
    Behaviors.setup(context => new CurrencyConverterManager(context))

  sealed trait Command
  final case class ConvertMessage(messageToConvert: MessageToConvert, replyTo: ActorRef[MessageConverted])
      extends CurrencyConverterManager.Command
      with ExchangeRate.Command
  final case class GetExchangeRate(
      currency: String,
      date: String,
      replyTo: ActorRef[ExchangeRateResponse]
  ) extends CurrencyConverterManager.Command
      with ExchangeRate.Command

  final case class MessageConverted(
      originalMessage: MessageToConvert,
      convertedMessage: Either[String, MessageToConvert]
  )
  final case class ExchangeRateResponse(
      exchangeRate: Either[String, FetchResponse]
  )

  private final case class ExchangeRateMapKey(targetCurrency: String, date: String, baseCurrency: String = "EUR")
  private final case class ExchangeRateMapValue(
      exchangeRateActor: ActorRef[ExchangeRate.Command],
      createdDateTime: DateTime
  )
}

class CurrencyConverterManager(
    context: ActorContext[CurrencyConverterManager.Command]
) extends AbstractBehavior[CurrencyConverterManager.Command](context) {
  import CurrencyConverterManager._

  implicit val system: ActorSystem[Nothing] = context.system
  // needed for the future flatMap/onComplete in the end
  implicit val executionContext: ExecutionContextExecutor = system.executionContext

  private val exchangeRateTtl      = 2.hours.toMillis
  private var exchangeRatesPerDate = Map.empty[ExchangeRateMapKey, ExchangeRateMapValue]

  context.log.info("CurrencyConverterService started")

  override def onMessage(msg: Command): Behavior[Command] = {
    msg match {
      case convertMsg @ ConvertMessage(messageToConvert, _) =>
        val key               = ExchangeRateMapKey(messageToConvert.currency, messageToConvert.isoDate)
        val exchangeRateActor = getOrCreateExchangeRateActor(key)
        exchangeRateActor ! convertMsg
        this

      case getExchangeRateMsg @ GetExchangeRate(currency, date, _) =>
        val key               = ExchangeRateMapKey(currency, date)
        val exchangeRateActor = getOrCreateExchangeRateActor(key)
        exchangeRateActor ! getExchangeRateMsg
        this
    }
  }

  private def getOrCreateExchangeRateActor(key: ExchangeRateMapKey): ActorRef[ExchangeRate.Command] = {
    exchangeRatesPerDate.get(key) match {
      case Some(ExchangeRateMapValue(exchangeRateActor, createdDateTime)) =>
        if ((DateTime.now - exchangeRateTtl).compare(createdDateTime) > 0) {
          exchangeRateActor ! ExchangeRate.Passivate
          createExchangeRateActor(key)
        } else {
          exchangeRateActor
        }
      case None => createExchangeRateActor(key)
    }
  }

  private def createExchangeRateActor(key: ExchangeRateMapKey): ActorRef[ExchangeRate.Command] = {
    context.log.info("Creating actor for {}", key)
    val createdDateTime = DateTime.now
    val exchangeRateActor =
      context.spawn(
        ExchangeRate(key.baseCurrency, key.targetCurrency, key.date),
        s"exchange-rate-${key.baseCurrency}-${key.targetCurrency}-${key.date}-$createdDateTime"
      )
    exchangeRatesPerDate += key -> ExchangeRateMapValue(exchangeRateActor, createdDateTime)
    exchangeRateActor
  }

  override def onSignal: PartialFunction[Signal, Behavior[Command]] = {
    case PostStop =>
      context.log.info("CurrencyConverterManager stopped")
      this
  }

}
