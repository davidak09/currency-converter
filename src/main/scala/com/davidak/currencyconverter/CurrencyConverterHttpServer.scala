package com.davidak.currencyconverter

import akka.actor.typed.scaladsl.AskPattern._
import akka.actor.typed.{ActorRef, ActorSystem}
import akka.http.scaladsl.Http
import akka.http.scaladsl.model.StatusCodes
import akka.http.scaladsl.server.Directives.{get, _}
import akka.http.scaladsl.server.StandardRoute
import akka.util.Timeout
import com.davidak.currencyconverter.MessagesToConvert.{SimpleMessage, TradeMessage}

import scala.concurrent.duration._
import scala.concurrent.{ExecutionContext, Future}
import scala.io.StdIn

object CurrencyConverterHttpServer {

  // these are from spray-json
  import JsonFormats._

  def main(args: Array[String]): Unit = {
    implicit val system: ActorSystem[CurrencyConverterManager.Command] =
      ActorSystem(CurrencyConverterManager.apply(), "exchange-rates")
    // needed for the future flatMap/onComplete in the end
    implicit val executionContext: ExecutionContext = system.executionContext

    implicit val timeout: Timeout = 5.seconds

    val exchangeRateService: ActorRef[CurrencyConverterManager.Command] = system

    def convertMessage(
        messageToConvert: MessagesToConvert.MessageToConvert
    ): Future[CurrencyConverterManager.MessageConverted] =
      exchangeRateService.ask(CurrencyConverterManager.ConvertMessage(messageToConvert, _))
    def getExchangeRate(currency: String, date: String): Future[CurrencyConverterManager.ExchangeRateResponse] =
      exchangeRateService.ask(CurrencyConverterManager.GetExchangeRate(currency, date, _))

    val route =
      concat(
        pathPrefix("api" / "v1" / "conversion") {
          concat(
            path("trade") {
              post {
                entity(as[MessagesToConvert.TradeMessage]) { tradeMessage =>
                  onSuccess(convertMessage(tradeMessage)) { convertMessageRoute }
                }
              }
            },
            path("simple") {
              post {
                entity(as[MessagesToConvert.SimpleMessage]) { simpleMessage =>
                  onSuccess(convertMessage(simpleMessage)) { convertMessageRoute }
                }
              }
            },
            path("rate") {
              get {
                parameters("currency", "date") { (currency, date) =>
                  onSuccess(getExchangeRate(currency, date)) { response =>
                    response.exchangeRate match {
                      case Left(errorMessage) =>
                        complete(
                          StatusCodes.InternalServerError,
                          s"Cannot read current exchange rates: $errorMessage"
                        )
                      case Right(fetchResponse) => complete(fetchResponse)
                    }
                  }
                }
              }
            }
          )
        },
        // AWS health check
        pathSingleSlash {
          get {
            complete("OK")
          }
        }
      )

    val port          = scala.util.Properties.envOrElse("SERVER_PORT", "8080").toInt
    val bindingFuture = Http().newServerAt("localhost", port).bind(route)
    println(s"Server online at http://localhost:$port/\nPress RETURN to stop...")
    StdIn.readLine() // let it run until user presses return
    bindingFuture
      .flatMap(_.unbind())                 // trigger unbinding from the port
      .onComplete(_ => system.terminate()) // and shutdown when done

  }

  private def convertMessageRoute(messageConverted: CurrencyConverterManager.MessageConverted): StandardRoute = {
    messageConverted.convertedMessage match {
      case Left(failedMessage) =>
        complete(
          StatusCodes.InternalServerError,
          s"Cannot convert given message: $failedMessage"
        )
      case Right(value) =>
        value match {
          case tradeMessage: TradeMessage   => complete(tradeMessage)
          case simpleMessage: SimpleMessage => complete(simpleMessage)
        }
    }
  }
}
