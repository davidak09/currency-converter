package com.davidak.currencyconverter

import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport
import akka.http.scaladsl.model.DateTime
import spray.json.{DefaultJsonProtocol, JsString, JsValue, RootJsonFormat}

object JsonFormats extends DefaultJsonProtocol with SprayJsonSupport {

  implicit object DateTimeJsonFormat extends RootJsonFormat[DateTime] {

    override def write(obj: DateTime): JsString = JsString(obj.toIsoDateTimeString())

    override def read(json: JsValue): DateTime =
      json match {
        case JsString(s) => DateTime.fromIsoDateTimeString(s).getOrElse(throw new Exception("Malformed datetime"))
        case _           => throw new Exception("Malformed datetime")
      }
  }

  implicit val tradeMessageFormat: RootJsonFormat[MessagesToConvert.TradeMessage] =
    jsonFormat6(
      MessagesToConvert.TradeMessage
    )

  implicit val simpleMessageFormat: RootJsonFormat[MessagesToConvert.SimpleMessage] =
    jsonFormat3(
      MessagesToConvert.SimpleMessage
    )

  implicit val fetchResponseFormat: RootJsonFormat[ExchangeRate.FetchResponse] =
    jsonFormat2(
      ExchangeRate.FetchResponse
    )

}
